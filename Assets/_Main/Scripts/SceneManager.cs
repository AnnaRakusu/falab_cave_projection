using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;

public class SceneManager : MonoBehaviour
{
    public List<MediaPlayer> Imprintings = new List<MediaPlayer>();
    public List<Light> spotLights = new List<Light>();

    private float lasting = 8f;
    private float randomTime = 1f;
    private float speed = 2f;
    private float lightIntensity = 100f;

    // Start is called before the first frame update
    void Start()
    {
        StoryTelling();
    }
    
    public void StoryTelling()
    {
        //shuffle the spot light
        spotLights.Shuffle(4);

        //light on
        for (int i = 0; i < Imprintings.Count; i++)
        {
            int tempI = i;
            DOVirtual.DelayedCall(randomTime * tempI + 1, () =>
            {
                spotLights[tempI].DOIntensity(lightIntensity, speed);
            });
        }

        //play video
        DOVirtual.DelayedCall(5f, () =>
        {
            for (int i = 0; i < Imprintings.Count; i++)
            {
                Imprintings[i].Play();
            }
        });

        //light off
        DOVirtual.DelayedCall(5f + lasting, () =>
        {
            for (int i = 0; i < Imprintings.Count; i++)
            {
                Imprintings[i].Stop();
                spotLights[i].DOIntensity(0, speed);
            }
        });

        //REPEAT
        DOVirtual.DelayedCall(lasting + speed +10f, () => {
            StoryTelling();
        });
    }
}