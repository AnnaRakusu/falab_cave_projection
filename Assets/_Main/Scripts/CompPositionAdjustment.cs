using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CompPositionAdjustment : MonoBehaviour
{
    public GameObject fatherObj;
    public Image fatherImg;
    public Text instructionText;
    
    private List<GameObject> compObjs = new List<GameObject>();
    public static bool boolPanelMode = false;
    private int compCount = 0;
    private float speed = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize DOTween
        DOTween.Init(true, true, LogBehaviour.Verbose).SetCapacity(200, 10);

        // Put childObjs into list
        for (int i = 0; i < fatherObj.transform.childCount; i++)
        {
            compObjs.Add(fatherObj.transform.GetChild(i).gameObject);
        }

        // Loading Saved Data
        LoadPreference();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && SpotlightPositionAdjustment.boolPanelMode == false)
        {
            // (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.P)
            boolPanelMode = !boolPanelMode;

            if (boolPanelMode == false)
            {
                SavePreference();

                fatherImg.DOFade(0f, 2f).SetEase(Ease.InOutSine);
                instructionText.DOFade(0f, 2f).SetEase(Ease.InOutSine);
            }
            else
            {
                fatherImg.DOFade(0.3f, 2f).SetEase(Ease.InOutSine);
                instructionText.DOFade(1f, 2f).SetEase(Ease.InOutSine);
            }
        }

        if (boolPanelMode)
        {
            //adjust position
            if (Input.GetKeyDown(KeyCode.W))
            {
                compObjs[compCount].transform.position += new Vector3(0, speed, 0);
            }

            else if (Input.GetKeyDown(KeyCode.S))
            {
                compObjs[compCount].transform.position += new Vector3(0, -speed, 0);
            }

            else if (Input.GetKeyDown(KeyCode.A))
            {
                compObjs[compCount].transform.position += new Vector3(-speed, 0, 0);
            }

            else if (Input.GetKeyDown(KeyCode.D))
            {
                compObjs[compCount].transform.position += new Vector3(speed, 0, 0);
            }

            else if (Input.GetKeyDown(KeyCode.Z))
            {
                compObjs[compCount].transform.position += new Vector3(0, 0, -speed);
            }

            else if (Input.GetKeyDown(KeyCode.X))
            {
                compObjs[compCount].transform.position += new Vector3(0, 0, speed);
            }

            //switch the comp
            else if (Input.GetKeyDown(KeyCode.B))
            {
                if (compCount > 0)
                {
                    compCount += -1;
                }
                else
                {
                    compCount = compObjs.Count - 1;
                }
            }

            else if (Input.GetKeyDown(KeyCode.N))
            {
                if (compCount < compObjs.Count - 1)
                {
                    compCount += 1;
                }
                else
                {
                    compCount = 0;
                }
            }
        }
    }

    void SavePreference()
    {
        for (int i = 0; i < fatherObj.transform.childCount; i++)
        {
            PlayerPrefs.SetFloat("compObjs[" + i + "]-x", compObjs[i].transform.position.x);
            PlayerPrefs.SetFloat("compObjs[" + i + "]-y", compObjs[i].transform.position.y);
            PlayerPrefs.SetFloat("compObjs[" + i + "]-z", compObjs[i].transform.position.z);
            Debug.Log(PlayerPrefs.GetFloat("compObjs[" + i + "]-x") + "|" + PlayerPrefs.GetFloat("compObjs[" + i + "]-y") + PlayerPrefs.GetFloat("compObjs[" + i + "]-z"));
        }
    }

    void LoadPreference()
    {
        if (PlayerPrefs.HasKey("compObjs[0]-x") && PlayerPrefs.HasKey("compObjs[0]-y") && PlayerPrefs.HasKey("compObjs[0]-z"))
        {
            Debug.Log("Adjusted");
            for (int i = 0; i < fatherObj.transform.childCount; i++)
            {
                compObjs[i].transform.position = new Vector3(PlayerPrefs.GetFloat("compObjs[" + i + "]-x"), PlayerPrefs.GetFloat("compObjs[" + i + "]-y"), PlayerPrefs.GetFloat("compObjs[" + i + "]-z"));
            }
        }
    }
}
